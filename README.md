[![pipeline status](https://gitlab.com/miraldo.ramos/kube-news/badges/main/pipeline.svg)](https://gitlab.com/miraldo.ramos/kube-news/-/commits/main)
[![Docker Pulls](https://badgen.net/docker/pulls/miraldo/kube-news?icon=docker&label=pulls)](https://hub.docker.com/r/miraldo/kube-news/)
[![Docker Image Size](https://badgen.net/docker/size/miraldo/kube-news?icon=docker&label=image%20size)](https://hub.docker.com/r/miraldo/kube-news/)

# kube-news

